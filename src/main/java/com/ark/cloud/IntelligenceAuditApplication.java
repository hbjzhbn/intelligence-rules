package com.ark.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.ark.cloud.*")
public class IntelligenceAuditApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntelligenceAuditApplication.class, args);
	}

}
