package com.ark.cloud.modules.rules.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.ark.cloud.modules.rules.entity.RulesEntity;
import com.ark.cloud.modules.rules.service.RulesService;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

@Service("RulesService")
public class RulesServiceImpl implements RulesService{
	@Autowired
	private com.ark.cloud.modules.rules.dao.RulesDao RulesDao;
	/**
	 * 插入数据到数据库
	 */
	@Override
	public String InsertRulesEntity(RulesEntity RulesEntity,String[] doc) {
		Date date=new Date();
		RulesEntity.setCreateTime(date);
		int rows= RulesDao.insert(RulesEntity);
		System.out.println("插入"+rows+"条");
		return "OK";
	}
	/**
	 * 通过id值查询
	 */
	@Override
	public RulesEntity FindRulesEntityById(Long id) {
		//参数校验
		if(id==null||id<1)
			 throw new IllegalArgumentException("id值无效");
		//查询数据库信息
		RulesEntity entity = RulesDao.selectById(id);
		
		return entity;
	}
	/**
	 * 通过ID删除数据
	 */
	@Override
	public int DeleteRulesEntiryById(Long id) {
		//参数校验
		if(id==null||id<1)
			 throw new IllegalArgumentException("id值无效");
		//删除所选择信息
		int rows = RulesDao.deleteById(id);
		return rows;
	}
	/**
	 * 通过ID更新数据
	 * 
	 */
	@Override
	public int UpdateRulesEntityById(RulesEntity entity,Long id) {
		//1.参数校验
			if(entity==null)
				throw new IllegalArgumentException("保存对象不能为空");
		//2.查看参数是否填写完整
		//3.对数据进行保存
			UpdateWrapper<RulesEntity> updateWrapper = new UpdateWrapper();
			updateWrapper.eq("id", id);
			int rows = RulesDao.update(entity, updateWrapper);
		return rows;
	}
	@Override
	public RulesEntity SelectRulesByKeyWord(String keyWords, int pages) {
		// TODO Auto-generated method stub
		return null;
	}

}

