package com.ark.cloud.modules.rules.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;
/**
 * 
 * 规章制度
 * @author Houbonan
 *
 */
@Data
@TableName("audit_tbl_knowledgebase")
@Accessors(chain = true)
public class RulesEntity implements Serializable{
	public RulesEntity(long l, String string, String string2, String string3, String string4, String string5,
			long m, Date date) {
		// TODO Auto-generated constructor stub
	}
	public RulesEntity() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 版本号
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@TableId
	/**
	 * 设置主键递增
	 */
	@Id
	private Long id;
	/**
	 * 知识库类型
	 * type = FieldType.Keyword不会为该字段添加索引
	 */
	@Field(type = FieldType.Keyword)
	private Short baseType;
	/**
	 * 专业分类
	 * analyzer = "ik_max_word" 添加中文索引 
	 * type =FieldType.Text为字段添加Text索引  
	 */
	@Field(analyzer = "ik_max_word",type =FieldType.Text )
	private Short baseCategory;
	/**
	 * 标签
	 */
	@Field(analyzer = "ik_max_word",type =FieldType.Text )
	private String baseLabel;
	/**
	 * 知识库内容
	 */
	@Field(analyzer = "ik_max_word",type =FieldType.Text )
	private String baseContent;
	/**
	 * 附件路径
	 */
	@Field(type = FieldType.Keyword)
	private String basePath;
	/**
	 * 创建者操作人
	 */
	@Field(type = FieldType.Keyword)
	private Long createUserId;
	/**
	 * 创建时间
	 */
	@Field(type = FieldType.Keyword)
	private Date createTime;
	
	
	
	
}
