package com.ark.cloud.modules.rules.service;

import com.ark.cloud.modules.rules.entity.RulesEntity;

public interface RulesService {
	public String InsertRulesEntity(RulesEntity rulesEntity, String[] doc);

	public RulesEntity FindRulesEntityById(Long id);

	public int DeleteRulesEntiryById(Long id);

	public int UpdateRulesEntityById(RulesEntity entity, Long id);

	public RulesEntity SelectRulesByKeyWord(String keyWords, int pages);
}
