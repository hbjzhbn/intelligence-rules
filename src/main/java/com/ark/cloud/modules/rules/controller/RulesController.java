package com.ark.cloud.modules.rules.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ark.cloud.modules.rules.entity.RulesEntity;
import com.ark.cloud.modules.rules.service.RulesService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(description = "规章制度")
@RestController
@RequestMapping("/Rules")
public class RulesController {
	@Autowired
	private RulesService RulesService;
	/**
	 * 插入数据
	 * @return
	 */
	@ApiOperation(value = "新增规章",notes = "新增")
	@PostMapping("InsertRules")
	public String InsertRulesEntity(RulesEntity RulesEntity,String[] doc) {
		
		return RulesService.InsertRulesEntity(RulesEntity,doc);
	}
	/**
	 * 通过ID查询数据
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "查询规章",notes ="通过ID查询" )
	@GetMapping("SelectRulesById/{id}")
	public RulesEntity selectRulesEntityById(@PathVariable Long id) {
		return RulesService.FindRulesEntityById(id);
	}
	
	/**
	 * 通过ID删除数据
	 * 
	 */
	@ApiOperation(value = "删除数据",notes = "通过ID删除数据")
	@PostMapping("DeleteRulesById/{id}")
	public int DeleteRulesEntiryById(@PathVariable Long id) {
		return RulesService.DeleteRulesEntiryById(id);
	}
	/**
	 * 通过ID更新数据
	 * 
	 */
	@ApiOperation(value = "更新规章",notes ="通过ID更新" )
	@PostMapping("/UpdateRulesById/{id}")
	public int updatRulesEntityById(RulesEntity entity,@PathVariable Long id) {
		return RulesService.UpdateRulesEntityById(entity,id);
	}
	/**
	 * 根据关键字查询同时进行分页
	 */
	@ApiOperation(value = "通过关键字查询规章",notes ="通过关键字查询" )
	@PostMapping("SelectRulesByKeyWord")
	public RulesEntity SelectRulesByKeyWord( int Pages,String KeyWords) {
		return RulesService.SelectRulesByKeyWord(KeyWords,Pages);
	}
	
}
