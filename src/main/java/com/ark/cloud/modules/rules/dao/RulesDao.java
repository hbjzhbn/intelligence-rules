package com.ark.cloud.modules.rules.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ark.cloud.modules.rules.entity.RulesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface RulesDao extends BaseMapper<RulesEntity>{

}
