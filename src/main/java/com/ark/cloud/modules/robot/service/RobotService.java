package com.ark.cloud.modules.robot.service;

import java.util.List;

import com.ark.cloud.modules.rules.entity.RulesEntity;

public interface RobotService {

	List<RulesEntity> FindLawsContent(String type);

	List<RulesEntity> FindStateContent(String type);

	String AddLaws(RulesEntity rulesEntity);

	RulesEntity FindRecommand(String[] content, String action);

	RulesEntity FindContentRecommand(String[] content);

	RulesEntity FindHotRecommand();
	
}
