package com.ark.cloud.modules.robot.controller;

import java.util.List;

import javax.swing.text.AbstractDocument.Content;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ark.cloud.modules.robot.service.RobotService;
import com.ark.cloud.modules.rules.entity.RulesEntity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RequestMapping("/Robot")
@Api(description = "机器人")
@RestController()
public class RobotController {
	@Autowired 
	private RobotService robotservice; 
	
	@GetMapping("/LawsContent/{Type}")
	@ApiOperation(value = "查询法规",notes ="通过得到type检索-2" )
	public List<RulesEntity> FindLawsContent(@PathVariable String Type ) {
		return robotservice.FindLawsContent(Type);
	}
	
	@GetMapping("/StateContent/{Type}")
	@ApiOperation(value = "查询国网政策",notes ="通过得到type检索-3" )
	public List<RulesEntity> FindStateContent(@PathVariable String Type ) {
		return robotservice.FindStateContent(Type);
	}
	
	@PostMapping("/Recommend")
	@ApiOperation(value = "查询相关推荐",notes ="根据搜索内容及用户行为在相关推荐中展示" )
	public RulesEntity Recommend( String[] content,String action) {
		return robotservice.FindRecommand(content,action);
	}
	@PostMapping("/ContentRecommand")
	@ApiOperation(value = "查询内容推荐",notes ="根据跳转界面中的内容进行推荐" )
	public RulesEntity ContentRecommend( String[] content) {
		return robotservice.FindContentRecommand(content);
	}
	@PostMapping("/HotRecommend")
	@ApiOperation(value = "查询热门推荐",notes ="根据最近次数最多推荐中展示" )
	public RulesEntity HotRecommend() {
		return robotservice.FindHotRecommand();
	}
	
	@PostMapping("AddLaws")
	@ApiOperation(value = "录入法律法规" )
	public String AddLaws(RulesEntity rulesEntity) {
		return robotservice.AddLaws(rulesEntity);
				
	}
	
	
//	@GetMapping("")
//	@ApiOperation(value = "热门推荐")
//	public 
	
}
