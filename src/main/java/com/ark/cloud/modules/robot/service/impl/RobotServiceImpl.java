package com.ark.cloud.modules.robot.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ark.cloud.modules.robot.service.RobotService;
import com.ark.cloud.modules.rules.dao.RulesDao;
import com.ark.cloud.modules.rules.entity.RulesEntity;
@Service
public class RobotServiceImpl implements RobotService{
	@Autowired
	private RulesDao rulesDao;
	@Override
	public List<RulesEntity> FindLawsContent(String type) {
		Map<String, Object> params=new HashMap<>();
		params.put("baseType", 2);
		List<RulesEntity> Law = rulesDao.selectByMap(params);
		
		return Law;
	}
	
	@Override
	public List<RulesEntity> FindStateContent(String type) {
		Map<String, Object> params=new HashMap<>();
		params.put("baseType", 3);
		List<RulesEntity> State = rulesDao.selectByMap(params);
		
		return State;
	}

	@Override
	public String AddLaws(RulesEntity rulesEntity) {
		rulesDao.insert(rulesEntity);
		return "OK";
	}

	@Override
	public RulesEntity FindRecommand(String[] content, String action) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RulesEntity FindContentRecommand(String[] content) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RulesEntity FindHotRecommand() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
